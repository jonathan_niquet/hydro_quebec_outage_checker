#!/usr/bin/python3

from datetime import datetime
import json
import logging
import platform
import requests
import sys
from haversine import haversine
import logging_loki
import pgeocode


"""
"pannes": [
    [
        17, # Number of customers affected
        "2021-05-22 11:06:24", # Start of the outage
        "2021-05-22 19:00:00", # Planned end of the outage
        "P", # p = outage, i = planned outage
        "[-72.96841792931004, 45.79889300417661]", # Coordinates
        "L", # L,A,N,R (L = Working on it, A = Assigned, N = No Detail, R = On their way)
        "1", # I'm not sure what this is.
        "11", # Cause ID
        "7311", # I'm not sure what this is
        ""
    ],
"""

MY_POSTAL_CODE = "H1V"
DISTANCE_TO_MONITOR = 10
LOKI_INSTANCE = "http://192.168.50.11:3100/loki/api/v1/push"


handler = logging_loki.LokiHandler(
    url=LOKI_INSTANCE, 
    tags={"application": "hydro", "host": platform.node()},
    version="1",
)

logger = logging.getLogger("hydro")
logger.setLevel(logging.INFO)
logger.addHandler(handler)


def get_current_outages():
    # Return a list of current outages
    r = requests.get(f"http://pannes.hydroquebec.com/pannes/donnees/v3_0/bisversion.json?timeStamp={datetime.now().strftime('%s')}")

    if r.status_code != 200:
        logger.error(f"Failed to get current outages. HTTP status code: {r.status_code}")
        sys.exit()

    time_param = r.text.strip("\"")
    r = requests.get(f"http://pannes.hydroquebec.com/pannes/donnees/v3_0/bismarkers{time_param}.json")
    return json.loads(r.text)['pannes']

def get_outages_coordinates(outage):
    # Return a list of the coordinates of the inspected outage
    coordinates_list = []
    list_of_floats = []
    coordinates = outage[4].strip('[]').split(',')
    list_of_floats.append(float(coordinates[1]))
    list_of_floats.append(float(coordinates[0]))
    coordinates_list.append(tuple(list_of_floats))
    return coordinates_list

def compare_coordinates(coordinates, my_coordinates):
    # Return distance between the two coordinates in kilometers
    return haversine(coordinates[0], my_coordinates)

def get_geolocalisation_for_postal_code():
    # Return a tuple of a list of the latitude and longitude
    lookup = pgeocode.Nominatim('ca')
    local_lookup = lookup.query_postal_code(MY_POSTAL_CODE)
    return tuple([local_lookup.latitude, local_lookup.longitude])

def _get_outage_status(outage):
    statuses = {'L': 'Working on it', 'A': 'Work assigned', 'R': 'Team on their way'}
    if statuses.get(outage):
        return statuses.get(outage)
    else:
        return 'No Status'

def iterate_outages(outages, my_coordinates):
    number_of_closeby_outages = 0
    for outage in outages:
        coordinates = get_outages_coordinates(outage)
        distance = compare_coordinates(coordinates, my_coordinates)
        if distance < DISTANCE_TO_MONITOR:
            number_of_closeby_outages += 1
            logger.error(f"Outage within {round(distance, 2)} kms of your location")
            logger.info(f"Number of customers affected: {outage[0]}, Status: {_get_outage_status(outage[5])}, Start Time: {outage[1]}, Planned End: {outage[2]}")
            logger.info(outage)
        elif distance < DISTANCE_TO_MONITOR*2:
            logger.warning(f"Outage within {round(distance, 2)} kms of your location")
    if number_of_closeby_outages == 0:
        logger.info(f"Nothing to report within {DISTANCE_TO_MONITOR} kms")

def main():
    outages = get_current_outages()
    my_coordinates = get_geolocalisation_for_postal_code()
    iterate_outages(outages, my_coordinates)

if __name__ == "__main__":
    main()